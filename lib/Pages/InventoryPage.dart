import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:is_pos/Widgets/DrawerPage.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:is_pos/ScopedModels/main_scope.dart';

class InventoryPage extends StatefulWidget {
  final MainScope _model;

  InventoryPage(this._model);

  @override
  _InventoryPageState createState() => _InventoryPageState();
}

class _InventoryPageState extends State<InventoryPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Map<String, dynamic> _inventoryDetails = {
    'product_id': null,
    'quantity': null,
  };

  Future postInventory(MainScope model) async {
    String response =
        await widget._model.postInventory(model, _inventoryDetails);

    if (response == '1') {
      return showDialog<Null>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return new AlertDialog(
            title: new Text('Success'),
            content: new SingleChildScrollView(
              child: new ListBody(
                children: <Widget>[new Text('Inventory Updated')],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                child: Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } else {}
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('Failed'),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: <Widget>[new Text('Inventory could not be added')],
            ),
          ),
          actions: <Widget>[
            new FlatButton(
              child: Text('Close'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void initState() {
    super.initState();
    widget._model.getProduct(widget._model);
    widget._model.getInventory(widget._model);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Inventory'),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(
                text: 'Add To Inventory',
              ),
              Tab(
                text: 'Inventory',
              )
            ],
          ),
        ),
        drawer: MyDrawer(),
        body: TabBarView(children: <Widget>[
          Container(
            margin: EdgeInsets.all(30.0),
            child: Form(
              key: _formKey,
              child: ListView(
                children: <Widget>[
                  ScopedModelDescendant<MainScope>(
                    builder:
                        (BuildContext context, Widget child, MainScope model) {
                      List<DropdownMenuItem<String>> dropDownList = [];

                      model.shop_products.map((product) {
                        dropDownList.add(DropdownMenuItem(
                          child: Text(product['name'].toString()),
                          value: product['id'].toString(),
                        ));
                      }).toList();
//                        dropDownList.add(value)
//                        print(widget._model.shop_products.length.toString());

                      return DropdownButton(
                        items: dropDownList,
                        hint: Text('Select Product'),
                        value: _inventoryDetails['product_id'],
                        onChanged: (value) {
                          setState(() {
                            _inventoryDetails['product_id'] = value;
                          });
                        },
                      );
                    },
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(labelText: 'Quantity'),
                    onSaved: (String value) {
                      _inventoryDetails['quantity'] = value;
                    },
                    // initialValue: 'dvsituma@gmail.com',
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Product should not be empty';
                      }
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  ScopedModelDescendant<MainScope>(
                    builder:
                        (BuildContext context, Widget child, MainScope model) {
                      return RaisedButton(
                        color: Colors.redAccent,
                        child: Text(
                          'Add To Inventory',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            postInventory(model);
                          }
                          // Navigator.pushReplacementNamed(context, '/dashboard');
                        },
                      );
                    },
                  )
                ],
              ),
            ),
          ),
          ScopedModelDescendant<MainScope>(
              builder: (BuildContext context, Widget child, MainScope model) {
            return Container(
              child: ListView.builder(
                itemBuilder: (BuildContext context, index) => Card(
                      child: Container(
                        child: ListTile(
                          title: Text(model.shop_inventory[index]['name']),
                          subtitle:
                              Text('Quantity : ' + model.shop_inventory[index]['quantity'].toString()),
                        ),
                      ),
                    ),
                itemCount: model.shop_inventory.length,
              ),
            );
          })
        ]),
      ),
    );
  }
}
