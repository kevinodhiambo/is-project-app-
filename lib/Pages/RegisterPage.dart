import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:is_pos/ScopedModels/main_scope.dart';
import 'package:http/http.dart' as http;

class RegisterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _RegisterPage();
  }
}

class _RegisterPage extends State<RegisterPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Map<String, dynamic> _userDetails = {
    'email': null,
    'name': null,
    'first_name': null,
    'last_name': null,
    'town': null,
    'password': null,
  };

  Future doRegister(MainScope model) async {
    final Register = json.encode(_userDetails);
    http.Response response = await http.post('http://10.0.2.2:8000/users/',
        body: Register, headers: {'Accept': 'application/json', 'Content-Type': 'application/json'});
    if (response.statusCode < 400) {
//      Map<String, dynamic> convertedResponse = json.decode(response.body);
      return showDialog<Null>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return new AlertDialog(
            title: new Text('Registration Successfull'),
            content: new SingleChildScrollView(
              child: new ListBody(
                children: <Widget>[new Text('Login with the details just registered with')],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                child: Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
//       print(convertedResponse);
      Navigator.pushNamed(context, '/login');
    } else {
      final Map<String, dynamic> authResponse = json.decode(response.body);
//      print(authResponse['username']);
      return showDialog<Null>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return new AlertDialog(
            title: new Text('Error Registering'),
            content: new SingleChildScrollView(
              child: new ListBody(
                children: <Widget>[new Text(authResponse['username'][0])],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                child: Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
        margin: EdgeInsets.all(30.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 150.0,
              ),
              Center(
                child: Text(
                  'Register',
                  style: TextStyle(fontSize: 40.0, fontWeight: FontWeight.w600),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'First Name'),
//                initialValue: 'Trial',
                onSaved: (String value) {
                  _userDetails['first_name'] = value;
                },
                // initialValue: 'dvsituma@gmail.com',
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'First Name should not be empty';
                  }
                },
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Last Name'),
//                initialValue: 'trial',
                onSaved: (String value) {
                  _userDetails['last_name'] = value;
                },
                // initialValue: 'dvsituma@gmail.com',
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Last Name should not be empty';
                  }
                },
              ),
              TextFormField(
                keyboardType: TextInputType.emailAddress,
//                initialValue: 'trial@trial.com',
                decoration: InputDecoration(labelText: 'Email'),
                onSaved: (String value) {
                  _userDetails['email'] = value;
                },
                // initialValue: 'dvsituma@gmail.com',
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Email should not be empty';
                  }
                  if (!value.contains('@')) {
                    return 'Email be valid';
                  }
                },
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Shop Name'),
//                initialValue: 'trial',
                onSaved: (String value) {
                  _userDetails['name'] = value;
                },
                // initialValue: 'dvsituma@gmail.com',
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Shop Name should not be empty';
                  }
                },
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Town'),
//                initialValue: 'trial',
                onSaved: (String value) {
                  _userDetails['town'] = value;
                },
                // initialValue: 'dvsituma@gmail.com',
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Town should not be empty';
                  }
                },
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Password'),
//                  initialValue: '123456',
                // initialValue: 'd83SqrR5',
                obscureText: true,
                onSaved: (String value) {
                  _userDetails['password'] = value;
                },
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Password should not be empty';
                  }
                },
              ),
              SizedBox(
                height: 20.0,
              ),
              ScopedModelDescendant<MainScope>(
                builder: (BuildContext context, Widget child, MainScope model) {
                  return RaisedButton(
                    color: Colors.redAccent,
                    child: Text(
                      'Register',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        doRegister(model);
                      }
                      // Navigator.pushReplacementNamed(context, '/dashboard');
                    },
                  );
                },
              ),
              ScopedModelDescendant<MainScope>(
                builder: (BuildContext context, Widget child, MainScope model) {
                  return RaisedButton(
//                    color: Colors.redAccent,
                    child: Text(
                      'I already have an account? Login.',
//                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () => Navigator.pushNamed(context, '/login'),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
