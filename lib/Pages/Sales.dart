import 'package:flutter/material.dart';
import 'package:is_pos/Widgets/DrawerPage.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:is_pos/ScopedModels/main_scope.dart';

class SalesPage extends StatefulWidget {
  final MainScope _model;

  SalesPage(this._model);

  @override
  _SalesPageState createState() => _SalesPageState();
}

class _SalesPageState extends State<SalesPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Map<String, dynamic> _saleDetails = {
    'product_id': null,
    'quantity': null,
    'price': null,
    'free_items': null,
  };

  Future postSale(MainScope model) async {
    String response = await model.postSale(model, _saleDetails);

    if (response == '1') {
      return showDialog<Null>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return new AlertDialog(
            title: new Text('Success'),
            content: new SingleChildScrollView(
              child: new ListBody(
                children: <Widget>[new Text('Sale Recorded')],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                child: Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } else {
      return showDialog<Null>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return new AlertDialog(
            title: new Text('Failed'),
            content: new SingleChildScrollView(
              child: new ListBody(
                children: <Widget>[new Text('Sale could not be recorded')],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                child: Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  void initState() {
    super.initState();
    widget._model.getProduct(widget._model);
    widget._model.getSale(widget._model);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Sale'),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(
                text: 'Make Sale',
              ),
              Tab(
                text: 'Today\'s Sale',
              )
            ],
          ),
        ),
        drawer: MyDrawer(),
        body: TabBarView(children: <Widget>[
          Container(
            margin: EdgeInsets.all(30.0),
            child: Form(
              key: _formKey,
              child: ListView(
                children: <Widget>[
                  ScopedModelDescendant<MainScope>(
                    builder:
                        (BuildContext context, Widget child, MainScope model) {
                      List<DropdownMenuItem<String>> dropDownList = [];

//                      print(model.shop_products.length.toString());
                      model.shop_products.map((product) {
                        dropDownList.add(DropdownMenuItem(
                          child: Text(product['name'].toString()),
                          value: product['id'].toString(),
                        ));
                      }).toList();
//                        dropDownList.add(value)
//                        print(widget._model.shop_products.length.toString());

                      return DropdownButton(
                        items: dropDownList,
                        hint: Text('Select Product'),
                        value: _saleDetails['product_id'],
                        onChanged: (value) {
                          setState(() {
                            _saleDetails['product_id'] = value;
                          });
                        },
                      );
                    },
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(labelText: 'Quantity'),
                    onSaved: (String value) {
                      _saleDetails['quantity'] = value;
                    },
                    // initialValue: 'dvsituma@gmail.com',
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Product should not be empty';
                      }
                    },
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(labelText: 'Price'),
                    onSaved: (String value) {
                      _saleDetails['price'] = value;
                    },
                    // initialValue: 'dvsituma@gmail.com',
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Product should not be empty';
                      }
                    },
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(labelText: 'Free Items'),
                    onSaved: (String value) {
                      _saleDetails['free_items'] = value;
                    },
                    // initialValue: 'dvsituma@gmail.com',
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Product should not be empty';
                      }
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  ScopedModelDescendant<MainScope>(
                    builder:
                        (BuildContext context, Widget child, MainScope model) {
                      return RaisedButton(
                        color: Colors.redAccent,
                        child: Text(
                          'Record Sale',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            postSale(model);
                          }
                          // Navigator.pushReplacementNamed(context, '/dashboard');
                        },
                      );
                    },
                  )
                ],
              ),
            ),
          ),
          ScopedModelDescendant<MainScope>(
              builder: (BuildContext context, Widget child, MainScope model) {
            return Container(
              child: ListView.builder(
                itemBuilder: (BuildContext context, index) => Card(
                      child: Container(
                        child: ListTile(
                          leading: Text('Quantity : ' + model.shop_sale[index]['quantity'].toString()),
                          title: Text('Product : ' + model.shop_sale[index]['product_id']['name']),
                          subtitle: Text('Total : KES ' + (model.shop_sale[index]['quantity'] * model.shop_sale[index]['price']).toString()),
                        ),
                      ),
                    ),
                itemCount: model.shop_sale.length,
              ),
            );
          })
        ]),
      ),
    );
  }
}
