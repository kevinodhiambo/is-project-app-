import 'package:flutter/material.dart';
import 'package:is_pos/Widgets/DrawerPage.dart';

class ExpensesPage extends StatefulWidget {
  @override
  _ExpensesPageState createState() => _ExpensesPageState();
}

class _ExpensesPageState extends State<ExpensesPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Expenses'),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(
                text: 'Add Expense',

              ),
              Tab(
                text: 'Expense',
              )
            ],
          ),
        ),
        drawer: MyDrawer(),
        body: TabBarView(children: <Widget>[
          Container(),
          Container()
        ]),
      ),
    );
  }
}
