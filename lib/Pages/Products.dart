import 'package:flutter/material.dart';
import 'package:is_pos/Widgets/DrawerPage.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:is_pos/ScopedModels/main_scope.dart';

class ProductsPage extends StatefulWidget {
  final MainScope _model;

  ProductsPage(this._model);

  @override
  _ProductsPageState createState() => _ProductsPageState(_model);
}

class _ProductsPageState extends State<ProductsPage> {
  final MainScope _model;

  _ProductsPageState(this._model);

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Map<String, dynamic> _productData = {'name': null, 'description': null};

  void initState() {
    super.initState();
    _model.getProduct(_model);
  }

  Future postProduct(MainScope model) async {
    String response = await model.postProduct(model, _productData);

    if (response == '1') {
      return showDialog<Null>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return new AlertDialog(
            title: new Text('Success'),
            content: new SingleChildScrollView(
              child: new ListBody(
                children: <Widget>[new Text('Product Added')],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                child: Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } else {
      return showDialog<Null>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return new AlertDialog(
            title: new Text('Failed'),
            content: new SingleChildScrollView(
              child: new ListBody(
                children: <Widget>[new Text('Product could not be added')],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                child: Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Products'),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(
                text: 'Add Product',
              ),
              Tab(
                text: 'Products',
              )
            ],
          ),
        ),
        drawer: MyDrawer(),
        body: TabBarView(children: <Widget>[
          Container(
            margin: EdgeInsets.all(30.0),
            child: Form(
              key: _formKey,
              child: ListView(
                children: <Widget>[
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(labelText: 'Product Name'),
                    onSaved: (String value) {
                      _productData['name'] = value;
                    },
                    // initialValue: 'dvsituma@gmail.com',
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Product should not be empty';
                      }
                    },
                  ),
                  TextFormField(
                    maxLines: 5,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(labelText: 'Description'),
                    onSaved: (String value) {
                      _productData['description'] = value;
                    },
                    // initialValue: 'dvsituma@gmail.com',
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Product should not be empty';
                      }
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  ScopedModelDescendant<MainScope>(
                    builder:
                        (BuildContext context, Widget child, MainScope model) {
                      return RaisedButton(
                        color: Colors.redAccent,
                        child: Text(
                          'Add Product',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            postProduct(_model);
                          }
                          // Navigator.pushReplacementNamed(context, '/dashboard');
                        },
                      );
                    },
                  )
                ],
              ),
            ),
          ),
          ScopedModelDescendant<MainScope>(
              builder: (BuildContext context, Widget child, MainScope model) {
            return Container(
              child: ListView.builder(
                itemBuilder: (BuildContext context, index) => Card(
                      child: Container(
                        child: ListTile(
                          title: Text(model.shop_products[index]['name'].toString()),
                          subtitle: Text(model.shop_products[index]['description'].toString()),
                        ),
                      ),
                    ),
                itemCount: _model.shop_products.length,
              ),
            );
          })
        ]),
      ),
    );
  }
}
