import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:is_pos/ScopedModels/main_scope.dart';
import 'package:http/http.dart' as http;

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LoginPage();
  }
}

class _LoginPage extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Map<String, dynamic> _userDetails = {
    'username': null,
    'password': null,
  };

  Future doLogin(MainScope model) async {
    final login = json.encode(_userDetails);
    http.Response response = await http.post('http://10.0.2.2:8000/api/token/',
        body: login, headers: {'Accept': 'application/json', 'Content-Type': 'application/json'});
    if (response.statusCode < 400) {
      Map<String, dynamic> convertedResponse = json.decode(response.body);
//       print(convertedResponse);
      model.addAccessTokenAndSaveFcm(convertedResponse['access'], model);
      model.getUserDetails(model, convertedResponse['access']);
    } else {
      final Map<String, dynamic> authResponse = json.decode(response.body);
//      print(authResponse);
      return showDialog<Null>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return new AlertDialog(
            title: new Text('Failed Login Attempt'),
            content: new SingleChildScrollView(
              child: new ListBody(
                children: <Widget>[new Text(authResponse['non_field_errors'][0])],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                child: Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
        margin: EdgeInsets.all(30.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 150.0,
              ),
              Center(
                child: Text(
                  'Login',
                  style: TextStyle(fontSize: 40.0, fontWeight: FontWeight.w600),
                ),
              ),
              TextFormField(
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(labelText: 'Email'),
                onSaved: (String value) {
                  _userDetails['username'] = value;
                },
                // initialValue: 'dvsituma@gmail.com',
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Email should not be empty';
                  }
                  if (!value.contains('@')) {
                    return 'Email be valid';
                  }
                },
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Password'),
                //  initialValue: '7ICnlMgg',
                // initialValue: 'd83SqrR5',
                obscureText: true,
                onSaved: (String value) {
                  _userDetails['password'] = value;
                },
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Password should not be empty';
                  }
                },
              ),
              SizedBox(
                height: 20.0,
              ),
              ScopedModelDescendant<MainScope>(
                builder: (BuildContext context, Widget child, MainScope model) {
                  return RaisedButton(
                    color: Colors.redAccent,
                    child: Text(
                      'Login',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        doLogin(model);
                      }
                      // Navigator.pushReplacementNamed(context, '/dashboard');
                    },
                  );
                },
              ),
              ScopedModelDescendant<MainScope>(
                builder: (BuildContext context, Widget child, MainScope model) {
                  return RaisedButton(
//                    color: Colors.redAccent,
                    child: Text(
                      'Don\'t have an account? Register Now.',
//                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () => Navigator.pushNamed(context, '/register'),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
