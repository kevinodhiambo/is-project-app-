import 'package:flutter/material.dart';
import 'package:is_pos/Pages/Expenses.dart';
import 'package:is_pos/Pages/InventoryPage.dart';
import 'package:is_pos/Pages/LoginPage.dart';
import 'package:is_pos/Pages/Products.dart';
import 'package:is_pos/Pages/RegisterPage.dart';
import 'package:is_pos/Pages/Reports.dart';
import 'package:is_pos/Pages/Sales.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:is_pos/ScopedModels/main_scope.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  SharedPreferences.setMockInitialValues({});
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final MainScope _model = MainScope();

  @override

  void initState() {
    super.initState();
    _model.autoAuthenticate(_model);
  }

  Widget build(BuildContext context) {
    return ScopedModel(
        child:  MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
            primarySwatch: Colors.blue,
          ),
          routes: {
            '/': ((BuildContext context) => ScopedModelDescendant<MainScope>(
              builder: (BuildContext context, Widget child, MainScope model) {
                print(model.userDetails);
                // print('Main : ' + model.userDetails.toString());
                return model.userDetails == null
                    ? LoginPage()
                    : SalesPage(_model);
              },
            )),
//            '/': ((BuildContext context) => LoginPage()),
//            '/': ((BuildContext context) => SalesPage()),
            '/register': ((BuildContext context) => RegisterPage()),
            '/login': ((BuildContext context) => LoginPage()),
            '/products': ((BuildContext context) => ProductsPage(_model)),
            '/inventory': ((BuildContext context) => InventoryPage(_model)),
            '/reports': ((BuildContext context) => ReportsPage()),
            '/expenses': ((BuildContext context) => ExpensesPage()),
          },
    ),
    model: _model,
    );
  }
}
