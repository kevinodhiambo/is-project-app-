import 'package:flutter/material.dart';
import 'package:is_pos/ScopedModels/main_scope.dart';
import 'package:scoped_model/scoped_model.dart';

class MyDrawer extends StatefulWidget {
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainScope>(
        builder: (context, child, MainScope model) {
          if (model.userDetails != null) {
           return Drawer(
             child: ListView(
               children: <Widget>[
                 UserAccountsDrawerHeader(
                   currentAccountPicture: CircleAvatar(
                     backgroundColor: Colors.white,
                   ),
                   otherAccountsPictures: <Widget>[
                     CircleAvatar(
                       backgroundColor: Colors.white,
                     ),
                   ],
                   accountName: Text(model.userDetails['user']['first_name'].toString() + ' ' + model.userDetails['user']['last_name'].toString()),
                   accountEmail: Text(model.userDetails['shop']['name'].toString()),
                 ),
                 ListTile(
                   leading: Icon(Icons.monetization_on, color: Theme.of(context).accentColor,),
                   title: Text('Sales'),
                   onTap: () => Navigator.pushNamed(context, '/'),
                 ),
                 ListTile(
                   leading: Icon(Icons.list, color: Theme.of(context).accentColor,),
                   title: Text('Products'),
                   onTap: () => Navigator.pushNamed(context, '/products'),
                 ),
                 ListTile(
                   leading: Icon(Icons.format_list_numbered, color: Theme.of(context).accentColor,),
                   title: Text('Inventory'),
                   onTap: () => Navigator.pushNamed(context, '/inventory'),
                 ),
                 ListTile(
                   leading: Icon(Icons.show_chart, color: Theme.of(context).accentColor,),
                   title: Text('Reports'),
                   onTap: () => Navigator.pushNamed(context, '/reports'),
                 ),
//                 ListTile(
//                   leading: Icon(Icons.money_off, color: Theme.of(context).accentColor,),
//                   title: Text('Expenses'),
//                   onTap: () => Navigator.pushNamed(context, '/expenses'),
//                 ),
                 ListTile(
                   leading: Icon(Icons.power_settings_new, color: Theme.of(context).accentColor,),
                   title: Text('Logout'),
                   onTap: () {
                     model.doLogout(model);
                   },
                 ),
               ],
             ),
           );
         } else {
           return Container();
         }
        });
  }
}
