import 'dart:convert';

import 'package:is_pos/ScopedModels/main_scope.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart' as http;

class InventoryScope extends Model {

  List _inventory = [];

  List get shop_inventory {
    return _inventory;
  }

  Future postInventory (MainScope model, Map<String, dynamic> inventoryData) async {
    final body = json.encode(inventoryData);
    http.Response response = await http.post(model.inventoryUrls['post_inventory'], body: body, headers: model.fullHeaderPost );

    if (response.statusCode < 400) {
      this.getInventory(model);
      return '1';
    } else {
      return '2';
    }
  }

  Future getInventory (MainScope model) async {
    http.Response response = await http.get(model.inventoryUrls['get_inventory'], headers: model.fullHeaderGet );
//    print(response.body);
    _inventory = json.decode(response.body);
    notifyListeners();
  }

}