import 'dart:async';
import 'dart:convert';

import 'package:scoped_model/scoped_model.dart';
import 'package:is_pos/ScopedModels/main_scope.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class AuthScope extends Model {
  Map<String, dynamic> _userDetails;
  String _fcm;
  bool _loginStatus = false;

  Map<String, dynamic> get userDetails {
    return _userDetails;
  }

  void addFcm(String fcm) {
    this._fcm = fcm;
  }

  bool get loginStatus  {
    return this._loginStatus;
  }

  void loginTrue() {
    this._loginStatus = true;
    notifyListeners();
  }

  void loginFalse() {
    this._loginStatus = false;
    notifyListeners();
  }

  String get fcm {
    return _fcm;
  }

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();


  void getUserDetails(MainScope model, String accessToken) async {
//     print(model.authUrls['get_user']);
    http.Response response = await http.get(model.authUrls['get_user'],
        headers: model.fullHeaderGet);
    final Map<String, dynamic> _convertedData = json.decode(response.body);
//    print(_convertedData);
//    model.loginFalse();
    saveUserDataLocally(model, _convertedData, accessToken);
  }

  void saveUserDataLocally(MainScope model, Map<String, dynamic> userData,
      String accessToken) async {
    final SharedPreferences prefs = await _prefs;
    prefs.setString('access_token', accessToken);
    prefs.setString('user_data', json.encode(userData));
    addUserDataToState(model);

  }

  void addUserDataToState(MainScope model) async {
//    print('Adding to state');
    final SharedPreferences prefs = await _prefs;
    final accessToken = prefs.getString('access_token');
    final userData = json.decode(prefs.getString('user_data'));
    model.addAccessToken(accessToken);
    _userDetails = userData;
    autoUpdateData(model);
    notifyListeners();
  }

  void autoUpdateData(MainScope model) {
//    print('autoupdate');
  }



  void autoAuthenticate(MainScope model) async {
    // doLogout(model);

    final SharedPreferences prefs = await _prefs;
    final accessToken = prefs.getString('access_token');
    if (accessToken != null) {
      addUserDataToState(model);
    }
  }

  void doLogout(MainScope model) async {
    final SharedPreferences prefs = await _prefs;
    prefs.remove('access_token');
    prefs.remove('user_data');
    _userDetails = null;
    notifyListeners();
  }
}
