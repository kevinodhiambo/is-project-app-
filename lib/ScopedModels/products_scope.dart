import 'dart:convert';

import 'package:is_pos/ScopedModels/main_scope.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart' as http;

class ProductScope extends Model {

  List _products = [];

  List get shop_products {
    return _products;
  }

  Future postProduct (MainScope model, Map<String, dynamic> productData) async {
    final body = json.encode(productData);
    http.Response response = await http.post(model.productUrls['post_product'], body: body, headers: model.fullHeaderPost );

    if (response.statusCode < 400) {
      this.getProduct(model);
      return '1';
    } else {
      return '2';
    }
  }

  Future getProduct (MainScope model) async {
    http.Response response = await http.get(model.productUrls['get_products'], headers: model.fullHeaderGet );
//    print(response.body);
    _products = json.decode(response.body);
    notifyListeners();
  }

}