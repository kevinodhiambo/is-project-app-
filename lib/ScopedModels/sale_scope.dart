import 'dart:convert';

import 'package:is_pos/ScopedModels/main_scope.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart' as http;

class SaleScope extends Model {

  List _sale = [];

  List get shop_sale {
    return _sale;
  }

  Future postSale (MainScope model, Map<String, dynamic> saleData) async {
    final body = json.encode(saleData);
    http.Response response = await http.post(model.saleUrls['post_sale'], body: body, headers: model.fullHeaderPost );

    if (response.statusCode < 400) {
      this.getSale(model);
      return '1';
    } else {
      return '2';
    }
  }

  Future getSale (MainScope model) async {
    http.Response response = await http.get(model.saleUrls['get_sales'], headers: model.fullHeaderGet );
//    print(response.body);
    _sale = json.decode(response.body);
    notifyListeners();
  }

}