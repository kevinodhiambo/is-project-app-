import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:is_pos/ScopedModels/main_scope.dart';

class UrlScope extends Model {
   static String _mainUrl = 'http://10.0.2.2:8000/';
//  static String _mainUrl = 'http://b.walvel.com/';
  static String _mainApiUrl = _mainUrl + '';
  String _accessToken;

  String get accessToken {
    return _accessToken;
  }

  // Auth Urls
  final Map<String, dynamic> _authUrls = {
    'tokenOauth': _mainUrl + 'oauth/token',
    'get_user': _mainUrl + 'logged-user/',
    'post_fcm': _mainUrl + 'api/fcm'
  };

  Map<String, dynamic> get authUrls {
    return _authUrls;
  }

  // Products Urls
  final Map<String, dynamic> _productUrls = {
    'get_products': _mainApiUrl + 'products/',
    'post_product': _mainApiUrl + 'products/',
    'remove_product_from_business':
        _mainApiUrl + 'products/remove-from-business/',
  };

  Map<String, dynamic> get productUrls {
    return _productUrls;
  }

  // Orders Urls
  final Map<String, dynamic> _inventoryUrls = {
    'get_inventory': _mainApiUrl + 'inventory/',
    'post_inventory': _mainApiUrl + 'inventory/',
    'get_single_order': _mainApiUrl + 'orders/',
  };

  Map<String, dynamic> get inventoryUrls {
    return _inventoryUrls;
  }

  // Contacts Urls
  final Map<String, dynamic> _saleUrls = {
    'post_sale': _mainApiUrl + 'sales/',
    'get_sales': _mainApiUrl + 'sales/',
  };

  Map<String, dynamic> get saleUrls {
    return _saleUrls;
  }

  // Combo Urls
  final Map<String, dynamic> _comboUrls = {
    'get_new_contacts': _mainApiUrl + 'customers/new',
    'get_all_contacts': _mainApiUrl + 'customers',
    'get_contact': _mainApiUrl + 'customers/',
    'change_contact_status': _mainApiUrl + 'customers/status'
  };

  Map<String, dynamic> get comboUrls {
    return _comboUrls;
  }

  // Stats Urls
  final Map<String, dynamic> _statUrls = {
    'get_stats': _mainApiUrl + 'statistics',
  };

  Map<String, dynamic> get statUrls {
    return _statUrls;
  }

  // New Redirect Route
  String _newRedirectRoute;

  String get newRedirectRoute {
    return _newRedirectRoute;
  }

  void addNewRedirection(String route) {
    _newRedirectRoute = route;
    notifyListeners();
  }

// Old Redirect Route
  String _oldRedirectRoute;

  String get oldRedirectRoute {
    return _oldRedirectRoute;
  }

  void addOldRedirection(String route) {
    _oldRedirectRoute = route;
    notifyListeners();
  }

  // Navigation Manager
  void navigationManager(BuildContext context) {
    if (_newRedirectRoute != _oldRedirectRoute) {
//      print(_newRedirectRoute);
      Navigator.pushNamed(context, _newRedirectRoute);
      addOldRedirection(_newRedirectRoute);
    }
  }

  final _header = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  };

  var _fullHeaderGet;
  var _fullHeaderPost;

  void addAccessToken(String accessToken) {
    // print('LOG : Url Scope : ' + accessToken);
    _accessToken = accessToken;

    _fullHeaderGet = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + _accessToken
    };

    _fullHeaderPost = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + _accessToken
    };
    notifyListeners();
  }

  void addAccessTokenAndSaveFcm(String accessToken, MainScope model) {
    // print('LOG : Url Scope : ' + accessToken);
    _accessToken = accessToken;

    _fullHeaderGet = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + _accessToken
    };

    _fullHeaderPost = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + _accessToken
    };

    notifyListeners();
  }

  get fullHeaderGet {
    return _fullHeaderGet;
  }

  get fullHeaderPost {
    return _fullHeaderPost;
  }

  get header {
    return _header;
  }
}
