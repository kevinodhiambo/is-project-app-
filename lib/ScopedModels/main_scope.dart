import 'package:is_pos/ScopedModels/auth_scope.dart';
import 'package:is_pos/ScopedModels/inventory_scope.dart';
import 'package:is_pos/ScopedModels/products_scope.dart';
import 'package:is_pos/ScopedModels/sale_scope.dart';
import 'package:is_pos/ScopedModels/urls_scope.dart';
import 'package:scoped_model/scoped_model.dart';

class MainScope extends Model with AuthScope, UrlScope, ProductScope, InventoryScope, SaleScope {
  
}